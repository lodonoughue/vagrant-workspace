#!/bin/bash

mkdir -p \
    $HOME/.config \
    $HOME/.cache \
    $HOME/.local/share

stow --dir=dotfiles \
    zsh \
    vim

git clone --recursive \
    https://github.com/sorin-ionescu/prezto.git \
    "${HOME}/.config/zsh/.zprezto"

