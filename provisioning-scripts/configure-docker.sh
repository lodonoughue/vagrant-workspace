#!/bin/bash

sed -i "s@ExecStart=.*@ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0@" /lib/systemd/system/docker.service
systemctl daemon-reload
systemctl restart docker

