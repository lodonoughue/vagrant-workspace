
# map XDG directories to the 
# OS X default app data directories
export XDG_CONFIG_HOME=$HOME/.config
export XDG_DATA_HOME=$HOME/.local/share
export XDG_CACHE_HOME=$HOME/.cache

# create necessary folders
mkdir -p $XDG_DATA_HOME/zsh

# define variables
export ZDOTDIR=$XDG_CONFIG_HOME/zsh
export HISTFILE=$XDG_DATA_HOME/zsh/history
export PYLINTHOME=$XDG_CONFIG_HOME/python

# source the rest of the zprezto
# zshenv config
source $ZDOTDIR/.zprezto/runcoms/zshenv

